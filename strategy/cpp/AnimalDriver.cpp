#include <iostream>

#include "Animal.h"
#include "Dog.h"
#include "Bird.h"

using namespace std;

int main() {

  Animal * sparky = new Dog();
  Animal * tweety = new Bird();

  cout << "Dog : " << sparky -> tryToFly();
  cout << "\nBird : " << tweety -> tryToFly();

  // This allwos dynamic changes for flyingType
  sparky -> setFlyingAbility(new ItFlys());
  cout << "\nDog : " << sparky -> tryToFly() << "\n";

  // This allwos dynamic changes for flyingType
  tweety -> setFlyingAbility(new MaybeFly());
  cout << "\nBird : " << tweety -> tryToFly() << "\n";

  //tweety -> setName("Tegar");
  //cout << "\nBird : " << tweety -> getName() << "\n";


  return 0;
}
