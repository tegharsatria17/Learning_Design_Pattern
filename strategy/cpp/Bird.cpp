#include "Bird.h"

Bird::Bird(): Animal() {
  setSound("Tweet");
  setFlyingAbility(new ItFlys());
}

Bird::Bird(string name) : Animal(name){
  setSound("Tweet");
  setFlyingAbility(new ItFlys());
}

Bird::Bird(const Bird& b) : Animal(b) {
}
