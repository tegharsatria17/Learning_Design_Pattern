#include <typeinfo>
#include <iostream>

#include "Animal.h"

using namespace std;

Animal::Animal(){
  weight = 30;
  height = 100;
  speed = 20;
  favFood = "grass";
  name = "anyMal";
  sound = "shooo";
  flyingType = new CantFly(); // by default CantFly
}

Animal::Animal(string name){
   setName(name);
   weight = 30;
   height = 100;
   speed = 20;
   favFood = "grass";
   name = "anyMal";
   sound = "shooo";
   flyingType = new CantFly(); // by default CantFly
}

Animal::Animal(const Animal& a){
  setName(a.getName());
  setHeight(a.getHeight());
  setWeight(a.getWeight());
  setSpeed(a.getSpeed());
  setFavFood(a.getFavFood());
  setSound(a.getSound());
  setFlyingAbility(a.flyingType);
}

Animal::~Animal(){
  delete flyingType;
}

void Animal::setName(string newname){
  name = newname;
}

string Animal::getName() const {
  return name;
}

void Animal::setHeight(double newHeight){
  height = newHeight;
}

double Animal::getHeight() const{
  return height;
}

void Animal::setWeight(int newWeight){
  if (newWeight <= 0) {
    cout << "Weight must be bigger than 0\n";
  } else {
    weight = newWeight;    
  }
}

int Animal::getWeight() const{
  return weight;
}

void Animal::setFavFood(string newFavFood){
  favFood = newFavFood;
}

string Animal::getFavFood() const{
  return favFood;
}

void Animal::setSpeed(double newSpeed){
  speed = newSpeed;
}

double Animal::getSpeed() const{
  return speed;
}

void Animal::setSound(string newSound){
  sound = newSound;
}

string Animal::getSound() const{
  return sound;
}

string Animal::tryToFly() {
  return flyingType -> fly();
}

void Animal::setFlyingAbility(Flys* newFlyType) {

  /*
  if (typeid(*newFlyType) == typeid(ItFlys)){
    flyingType = new ItFlys();
  } else if (typeid(*newFlyType) == typeid(CantFly)){
    flyingType = new CantFly();
  }*/
  flyingType = newFlyType;
}

void Animal::print() {
  cout << "\n== ANIMAL DETAILS ==";
  cout << "\nName : " << name;
  cout << "\nWeight : " << weight;
  cout << "\nHeight : " << height;
  cout << "\nSpeed : " << speed;
  cout << "\nFav Food : " << favFood;
  cout << "\nSound : " << sound;
  cout << "\nFly : " << tryToFly() << "\n";
}
