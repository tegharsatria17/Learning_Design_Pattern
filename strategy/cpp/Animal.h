#ifndef _ANIMAL_H
#define _ANIMAL_H


#include <string>
#include "Flys.h"

using namespace std;

class Animal {

  private:
    int weight;
    double height;
    double speed;
    string favFood;
    string name;
    string sound;

  public:
    Flys* flyingType;

    Animal();
    Animal(string name);
    Animal(const Animal&);
    ~Animal();

    void setName(string newname);
    string getName() const;

    void setHeight(double newHeight);
    double getHeight() const;

    void setWeight(int newWeight);
    int getWeight() const;

    void setFavFood(string newFavFood);
    string getFavFood() const;

    void setSpeed(double newSpeed);
    double getSpeed() const;

    void setSound(string newSound);
    string getSound() const;

    string tryToFly();

    void setFlyingAbility(Flys* newFlyType);

    void print();


};


#endif
