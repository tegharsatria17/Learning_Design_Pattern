#ifndef _FLYS_H
#define _FLYS_H

#include <string>

using namespace std;

//abstrack base class
class Flys{
  public:
    virtual string fly() = 0;
};

class ItFlys : public Flys {
  public:
    string fly() {
      return string("Flying High");
    }
};

class CantFly : public Flys {
  public:
    string fly() {
      return string("I cant fly");
    }
};

class MaybeFly : public Flys {
  public:
    string fly() {
      return string("Hopefully can fly");
    }
};



#endif
