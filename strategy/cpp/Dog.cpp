#include <iostream>
#include "Dog.h"

using namespace std;


Dog::Dog() : Animal() {
  setSound("Bark");
  setFlyingAbility(new CantFly());
}

Dog::Dog(string name) : Animal(name) {
  setSound("Bark");
  setFlyingAbility(new CantFly());
}

Dog::Dog(const Dog& d) : Animal(d) {
}

void Dog::digHole() {
  cout << "Dug a hole\n";
}
