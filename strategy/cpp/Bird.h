#ifndef _BIRD_H
#define _BIRD_H

#include "Animal.h"

class Bird : public Animal{

  public:
    Bird();
    Bird(string name);
    Bird(const Bird&);
    ~Bird();
};

#endif
