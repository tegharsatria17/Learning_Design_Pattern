import Animal
from Animal.Flys import Flys, ItFlys, CantFly

if __name__ == '__main__':
    johnny = Animal.Animal()
    buddy = Animal.Dog()
    tweety = Animal.Bird()

    johnny.setFlyAbility(ItFlys)
    johnny.tryToFly()
    print tweety.sound

    buddy.tryToFly()
    buddy.setFlyAbility()
    buddy.tryToFly()
    tweety.tryToFly()

    print buddy.name
