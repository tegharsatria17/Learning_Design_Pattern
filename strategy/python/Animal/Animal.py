from Flys import Flys, ItFlys, CantFly

class Animal:
    
    def __init__(self):
        self.name = None
        self.height = None
        self.weight = None
        self.favFood = None
        self.speed = None
        self.sound = None
        self.flyAbility = Flys()

    def tryToFly(self):
        self.flyAbility.fly()

    def setFlyAbility(self, strategy=None):
        self.flyAbility = Flys(strategy)
        
class Dog(Animal):
    
    def __init__(self):
        Animal.__init__(self)
        self.sound = "Bark"
        self.flyAbility = Flys(CantFly)

class Bird(Animal):

    def __init__(self):
        Animal.__init__(self)
        self.sound = "Chirp"
        self.flyAbility = Flys(ItFlys)
        
