#!/usr/bin/python
import types

class Flys:

    def __init__(self, func=None):
        self.flyingType = 'Flying Type not set'
        if func is not None:
            self.fly = types.MethodType(func, self)

    def fly(self):
        print(self.flyingType)


def ItFlys(self):
    self.flyingType = 'Flying High'
    print (self.flyingType)

def CantFly(self):
    self.flyingType = "I can't fly"
    print (self.flyingType)

'''
if __name__ == '__main__':
    
    strat0 = Flys()

    strat1 = Flys(ItFlys)

    strat2 = Flys(CantFly)

    print strat1.flyingType
    
    strat0.fly()
    strat1.fly()
    strat2.fly()

    strat1 = Flys(CantFly)

    strat1.fly()
'''
